import React from 'react';
import './App.css';
import formStyles from './LoginForm.module.css';
import {
  BrowserRouter as Router,
  Route,
  Redirect
} from "react-router-dom";

class LoginForm extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  onSubmit() {
    console.log("Double bazinga!");
    this.props.onSubmit(this.state.username, this.state.password);
  }
  change (event) {
    const newState = Object.assign({}, this.state);
    newState[event.target.name] = event.target.value;
    this.setState(newState);
  }
  render() {
      return (
        <div className={formStyles.LoginPopup}>
          <button 
            className="ClosePopup"
            type="close"
            name="CloseLogin"
            onClick={this.props.onClose}
          >
            <img alt="closeButton" href="#"/>
          </button>
          <div className="LoginForm"> 
            <input className="LoginInput" type="text" name="username" id="username" placeholder="Username" onChange={this.change.bind(this)} />
            <input className="LoginInput" type="password" name="password"  id="password" placeholder="Password" onChange={this.change.bind(this)} />
            <button
              className="LoginButton"
              type="submit"
              name="login"
              onClick={this.onSubmit.bind(this)}>
                Log In
            </button>
          </div>
        </div>
      );
  }
}

class HomePage extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  onLoginFormClose() {
    this.setState({isLoginFormOpen:false});
  }

  openLoginForm () {
    console.log("bazinga!");
    const homePageState = {
      isLoginFormOpen: true
    };
    this.setState(homePageState);
  };

  render () {
    if (this.props.isLoggedIn) {
      return <Redirect to="/welcome" />
    }
    let loginForm;
    if (this.state["isLoginFormOpen"]) {
      loginForm = <LoginForm onClose={this.onLoginFormClose.bind(this)}
                             onSubmit={this.props.submitLoginForm}/>;
    }

    return <div className="App">
      <header className="App-header">
        <p>
          Welcome, Dude!
        </p>
        <button
          className="App-link"
          onClick={this.openLoginForm.bind(this)}
        >
          Log In
        </button>
        <div id="logIn">
          {loginForm}
        </div>
      </header>
    </div>
  };
}

class WelcomePage extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  render () {
    if (!this.props.isLoggedIn) {
      return <Redirect to="/" />
    }
    return <div>
        <p> Welcome, {this.props["username"]}!</p>
        <button
          className="App-link"
          onClick={this.props.logOut}
        >
          Log Out
        </button>
      </div>
  }
}

class App extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
  }
  submitLoginForm (username, password) {
    const appState = {
      username: username,
      password: password,
      isLoggedIn: true
    };

    console.log(username);
    console.log(password);
    console.log(appState);

    this.setState(appState);
  };
  logOut () {
    const appState = {
      isOpen: false,
      isLoggedIn: false
    };

    this.setState(appState);
  };
  render () {
    return <Router>
      <Route exact path="/">
          <HomePage
            submitLoginForm={this.submitLoginForm.bind(this)}
            isLoggedIn={this.state.isLoggedIn} />
      </Route>
      <Route path="/welcome">
        <WelcomePage
          username={this.state.username}
          logOut={this.logOut.bind(this)}
          isLoggedIn={this.state.isLoggedIn} />
      </Route> 
    </Router>
  }
}

export default App;
